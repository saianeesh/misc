import urllib.request
import sys



"""
    Get url text
    Also from Downloads/staff.py
"""
def url_get(url):
    stream = urllib.request.urlopen(url)
    text = stream.read().decode()
    stream.close()
    return text

"""
    returns the first definition for noun
    from Downloads/staff.py
"""
def find(string, text):
    str_len = len(string)
    pos = 0
    while pos < len(text):
        if text[pos:pos + str_len] == string:
            return pos + str_len
        pos += 1

    return None

"""
    Gets the definition
    i.e. gets the next quoted text
"""
def get_def(text, start_pos):
    end_pos = start_pos
    while text[end_pos] != '"':
        end_pos += 1
    
    return text[start_pos:end_pos]

"""
    Finds the first noun definition for the word using the
    'free dictionary api'
"""
def main():
    narg = len(sys.argv)
    if narg > 2:
        sys.exit("too many args")

    url_stem = "https://api.dictionaryapi.dev/api/v2/entries/en_US/"
    keyphrase = '"noun","definitions":[{"definition":"'
   
    text = url_get(url_stem + sys.argv[1])
    pos = find(keyphrase, text)
    if pos == None:
        sys.exit("keyphrase not found")
    
    definition = get_def(text, pos)
    print(definition)



if __name__ == "__main__":
    main()
