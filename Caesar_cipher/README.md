# Caesar cipher

A simple caesar cipher that shifts every alphabet in a string by the amount specified.


## Usage
```
./caesar <string_to_shift> <shift_amount>
```
Only alphabets are allowed in the string.

### Examples
`./caesar teststring 3`

